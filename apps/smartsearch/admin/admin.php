<?php
defined('SITEBILL_DOCUMENT_ROOT') or die('Restricted access');
/**
 * SmartSearch admin backend (https://bitbucket.org/dek4nice/smartsearch.sitebill)
 * @author Ilyenkov Dmitriy <d3k.web@gmail.com> http://www.ihos.ru
 */

class smartsearch_admin extends Object_Manager {

    function __construct( $realty_type = false ) {
        parent::__construct();

        require_once (SITEBILL_DOCUMENT_ROOT.'/apps/config/admin/admin.php');

        // Postinstall manual config installation
        if (true) {
            $config_admin = new config_admin();
            if ( !$config_admin->check_config_item('apps.smartsearch.enable') ) {
                $config_admin->addParamToConfig('apps.smartsearch.enable' , '0' , 'Включить приложение SmartSearch' , 1);
            }
            if ( !$config_admin->check_config_item('apps.smartsearch.title') ) {
                $config_admin->addParamToConfig('apps.smartsearch.title' , 'Поиск по объектам' , 'Заголовок формы' , 0);
            }
            if ( !$config_admin->check_config_item('apps.smartsearch.pages_enable') ) {
                $config_admin->addParamToConfig('apps.smartsearch.pages_enable' , '1' , 'Добавить в поиск страницы линк-менеджера' , 1);
            }
            if ( !$config_admin->check_config_item('apps.smartsearch.metro_enable') ) {
                $config_admin->addParamToConfig('apps.smartsearch.metro_enable' , '1' , 'Добавить поиск по метро' , 1);
            }
            if ( !$config_admin->check_config_item('apps.smartsearch.ajax_limit') ) {
                $config_admin->addParamToConfig('apps.smartsearch.ajax_limit' , '1000' , 'Лимит объектов при запросе' , 0);
            }
        }
        // $this->app_title = Multilanguage::_('APPLICATION_NAME','Smart Search');
        $this->app_title = 'Smart Search App';
        $this->action = 'smartsearch';
    }

    function _preload() {
        global $smarty;
        if ($this->getConfigValue('apps.smartsearch.enable')) {
            $smarty->assign('smart_search_form', $this->getSmartSearchTemplate());
        } else {
            $smarty->assign('smart_search_form', '');
        }
        return true;
    }

    function checkout_installed() {
        $DBC=DBC::getInstance();
        // $query="SELECT `component_id` FROM ".DB_PREFIX."_component WHERE `name`='smartsearchpills';";
        // $query="SELECT * FROM `information_schema`.`TABLES` WHERE `TABLE_SCHEMA`='u0085797_estate' AND `TABLE_NAME`='re_smartsearchpills'";
        $query="SELECT * FROM `information_schema`.`TABLES` WHERE `TABLE_NAME`='".DB_PREFIX."_smartsearchpills'";
        $stmt=$DBC->query($query);
        return (bool) $stmt;
    }

    function install() {
        $DBC=DBC::getInstance();
        $rs = '';
        $rs .= '<h3>'.Multilanguage::_('SQL_NOW','system').'</h3>';

        // re_config - apps.smartsearch.*
        $query="SELECT COUNT(id) AS _cnt FROM ".DB_PREFIX."_config WHERE config_key LIKE ?;";
        $stmt=$DBC->query($query , array('apps.smartsearch.%'));
        $ar=$DBC->fetch($stmt);
        if ( (int) $ar['_cnt']==0 ) {
            // CMS VERSION < 3.0
            // $query="INSERT INTO ".DB_PREFIX."_config (`config_key`,`value`,`title`,`sort_order`) VALUES (?,?,?,?)";
            // $stmt=$DBC->query($query, array('apps.smartsearch.enable','1','Включить приложение SmartSearch',1));
            // CMS VERSION >= 3.0
            $query="INSERT INTO ".DB_PREFIX."_config (`config_key`,`value`,`title`,`sort_order`,`vtype`) VALUES (?,?,?,?,?)";
            $stmt=$DBC->query($query, array('apps.smartsearch.enable','1','Включить приложение SmartSearch',1,1));
            $stmt=$DBC->query($query, array('apps.smartsearch.title','Поиск по объектам','Заголовок формы',2,0));
            $stmt=$DBC->query($query, array('apps.smartsearch.pages_enable','1','Добавить в поиск страницы линк-менеджера',3,1));
            $stmt=$DBC->query($query, array('apps.smartsearch.metro_enable','1','Добавить поиск по метро',4,1));
            $stmt=$DBC->query($query, array('apps.smartsearch.ajax_limit','1000','Лимит объектов при запросе',5,0));
            $rs .= 'Настройки инициализированны<br>';
        } else {
            // update settings checkbox
            $rs .= 'Настройки уже добавлены<br>';
            $query = "SELECT id FROM ".DB_PREFIX."_config WHERE `config_key`='apps.smartsearch.enable'";
            $stmt=$DBC->query($query);
            if ($stmt) {
                $ar=$DBC->fetch($stmt);
                $config_id = $ar['id'];
                $query = "UPDATE ".DB_PREFIX."_config SET `value`='1' WHERE `id`=?";
                $stmt=$DBC->query($query , array($config_id));
                $rs .= 'Модуль включен<br>';
            }
        }
        // $rs .= Multilanguage::_('QUERY_SUCCESS','system').': '.$query.'<br>';

        // re_table
        $query="SELECT table_id FROM ".DB_PREFIX."_table WHERE name=?";
        $stmt=$DBC->query($query , array('smartsearchpills'));
        if (!$stmt) {
            $query="INSERT INTO ".DB_PREFIX."_table (`name`,`description`) VALUES (?,?)";
            $stmt=$DBC->query($query, array('smartsearchpills','Smart Search Table'));
            $rs .= 'Внесено в реестр таблиц<br>';
        } else {$rs .= 'Запись в реестре таблиц уже существует<br>'; }
        // $rs .= Multilanguage::_('QUERY_SUCCESS','system').': '.$query.'<br>';

        // re_columns
        $query="SELECT * FROM ".DB_PREFIX."_table WHERE name='smartsearchpills'";
        $stmt=$DBC->query($query);
        if ($stmt) {
            $ar=$DBC->fetch($stmt);
            $table_id=$ar['table_id'];

            $query="SELECT COUNT(columns_id) AS _cnt FROM ".DB_PREFIX."_columns WHERE table_id=?";
            $stmt=$DBC->query($query, array($table_id));
            $ar=$DBC->fetch($stmt);
            if ($ar['_cnt']==0) {
                $query="INSERT INTO ".DB_PREFIX."_columns
                    (
                        `active`,`table_id`,`group_id`,`name`,`title`,
                        `primary_key_name`, `primary_key_table`, `query`, `value_name`, `title_default`, `value_default`,
                        `active_in_topic`,`tab`,`hint`,`entity`,
                        `type`,`sort_order`,`dbtype`,`parameters`
                    ) VALUES (
                        1,?,'0',?,?,
                        '','','','','','',
                        '0','',?,'',
                        ?,?,'','a:0:{}'
                    );";
                $stmt=$DBC->query($query, array($table_id, 'pill_id', 'ID', '', 'primary_key', 1));
                $stmt=$DBC->query($query, array($table_id, 'structure_ref', 'Категория', 'Биндинг группы (напр-р Офисы/Бизнес-центры)', 'select_box_structure', 2));
                $stmt=$DBC->query($query, array($table_id, 'label', 'Лейбл', 'латинницей, системное имя группы (напр-р office)', 'safe_string', 3));
                $stmt=$DBC->query($query, array($table_id, 'abrev', 'Аббревиатура', 'двух-четырёх символьное обозначение группы (напр-р Бц)', 'safe_string', 4));
                $stmt=$DBC->query($query, array($table_id, 'singular', 'Единственное число', '(напр-р Бизнес-центр)', 'safe_string', 5));
                $stmt=$DBC->query($query, array($table_id, 'plural', 'Множественное число', '(напр-р Офисы)', 'safe_string', 6));
                if ($stmt) {
                    $rs .= 'Колонки в модель [smartsearchpills] добавлены<br>';
                } else {
                    $rs .= 'Ошибка добавления колонки в модель [smartsearchpills]: '.$DBC->getLastError().'<br>';
                }
            } else {
                $rs .= 'Колонки для [smartsearchpills] уже существуют<br>';
            }
        } else {
            $rs .= 'Отсутствует модель [smartsearchpills]<br>';
        }
        // $rs .= Multilanguage::_('QUERY_SUCCESS','system').': '.$query.'<br>';

        // re_component
        $query="SELECT `component_id` FROM ".DB_PREFIX."_component WHERE `name`='smartsearchpills';";
        $stmt=$DBC->query($query);
        if (!$stmt) {
            $query="INSERT INTO ".DB_PREFIX."_component (`name`,`title`) VALUES ('smartsearchpills','Smart Search Pills');";
            $stmt=$DBC->query($query);
            $rs .= 'Компонент [smartsearchpills] добавлен<br>';
        } else {
            $rs .= 'Компонент [smartsearchpills] прописан<br>';
        }
        // $rs .= Multilanguage::_('QUERY_SUCCESS','system').': '.$query.'<br>';

        // re_customentity
        $query="SELECT `entity_name` FROM ".DB_PREFIX."_customentity WHERE `entity_name`='smartsearchpills';";
        $stmt=$DBC->query($query);
        if (!$stmt) {
            $query="INSERT INTO ".DB_PREFIX."_customentity (`entity_name`, `entity_title`) VALUES ('smartsearchpills','Smart Search');";
            $stmt=$DBC->query($query);
            $rs .= 'Пользовательская сущность [smartsearchpills] добавлена<br>';
        } else {
            $rs .= 'Пользовательская сущность [smartsearchpills] уже добавлена<br>';
        }
        // $rs .= Multilanguage::_('QUERY_SUCCESS','system').': '.$query.'<br>';

        // re_smartsearchpills
        $query = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."_smartsearchpills` (
            `pill_id` int(11) NOT NULL AUTO_INCREMENT,
            `structure_ref` int(10) unsigned NOT NULL DEFAULT '0',
            `label` varchar(255) NOT NULL DEFAULT '',
            `abrev` varchar(255) NOT NULL DEFAULT '',
            `singular` varchar(255) NOT NULL DEFAULT '',
            `plural` varchar(255) NOT NULL DEFAULT '',
            PRIMARY KEY (`pill_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=".DB_ENCODING." ;";
        $stmt=$DBC->query($query);
        $rs .= Multilanguage::_('QUERY_SUCCESS','system').': '.$query.'<br>';

        return $rs;
    }

    function install_example() {
        $DBC=DBC::getInstance();
        $query="INSERT INTO ".DB_PREFIX."_smartsearchpills (`structure_ref`,`label`,`abrev`,`singular`,`plural`) VALUES (?,?,?,?,?);";
        $stmt=$DBC->query($query , array(6121,'office','бц','Бизнес-центр','Офисы'));
        $stmt=$DBC->query($query , array(6122,'warehouse','ск','Склад','Склады'));
        $stmt=$DBC->query($query , array(6123,'trk','трк','ТРК','Торговые'));
        $stmt=$DBC->query($query , array(6125,'places','унв','Помещение','Помещения'));
        return '';
    }

    function uninstall() {
    }

    function main() {
        $html = '';
        $html .= $this->get_app_title_bar();
        switch( $this->getRequestValue('do') ){
            case 'install' :
                $html .= $this->install();
                $html .= '<br>';
                $html .= '<div>Приложение установлено</div>';
                $html .= '<dt><a href="/admin/index.php?action=smartsearch">Return</a></dt>';
                return $html;
                break;
            case 'install_example' :
                $html .= $this->install_example();
                $html .= '<br>';
                $html .= '<div>Демо данные [smartsearchpills] добавлены</div>';
                $html .= '<dt><a href="/admin/index.php?action=smartsearch">Return</a></dt>';
                return $html;
                break;
            case 'uninstall' :
                $html .= $this->uninstall();
                $html .= '<br>';
                $html .= '<div>Приложение удалено</div>';
                $html .= '<dt><a href="/admin/index.php?action=smartsearch">Return</a></dt>';
                return $html;
                break;
        }
        $html = '';
        $html .= '<h2>SmartSearch SiteBill CMS Plugin</h2>';
        $html .= '<hr>';
        if ($this->checkout_installed()) {
            $html .= '<h4>Main</h4>';
            $html .= '<dt><a href="/admin/?action=config">Settings</a></dt>';
            $html .= '<dt><a href="/admin/?action=smartsearchpills">Pills list</a></dt>';
            $html .= '<dt><a href="/admin/?action=table&do=edit&table_id=21">Edit table</a></dt>';
            $html .= '<h4>Service</h4>';
            $html .= '<dt><a href="/admin/index.php?action=smartsearch&do=uninstall">Uninstall</a></dt>';
            // $html .= '<dt><a href="/admin/index.php?action=smartsearch&do=install_example">Init demo pills</a></dt>';
        } else {
            $html .= '<br>';
            $html .= '<dt><a href="/admin/index.php?action=smartsearch&do=install">Install</a></dt>';
        }
        $html .= '<hr>';
        $html .= '<a href="https://bitbucket.org/dek4nice/smartsearch.sitebill/overview" target="_blank">Project homepage</a>';
        return $html;
    }

    protected function _defaultAction(){
    	return '';
    }

    protected function getPills() {
        $DBC=DBC::getInstance();
        $query="SELECT `structure_ref`,`label`,`abrev`,`singular`,`plural` FROM ".DB_PREFIX."_smartsearchpills ORDER BY `pill_id`;";
        return $DBC->query($query);
    }

    protected function setRequestedStateZZZZZZZZZ() {
        require_once(SITEBILL_DOCUMENT_ROOT . '/apps/smartsearch/lib/rpc.php');
        $this->state = new SmartSearchRpc();
        if (array_key_exists('id', $_GET)) {
            $id = intval($_GET['id']);
            if (array_key_exists($id , $this->realestate_base)) {
                if (array_key_exists('search', $_GET)) {
                    // search = $_GET['search'];
                    $this->state->set_data($this->realestate_base[$id]);
                } else {
                    $this->state->set_data($this->realestate_base[$id]);
                }
            } else {
                $this->state->set_error('ID not found');
            }
        } else {
            $this->state->set_error('GET:id required');
        }
        // return $state;
    }

    protected function getEstateLinks() {
        $DBC=DBC::getInstance();
        $query="SELECT `predefinedlinks_id`,`alias`,`title`,`meta_keywords`,`params` FROM ".DB_PREFIX."_predefinedlinks;";
        // var_dump($query); return;
        $stmt=$DBC->query($query);
        // var_dump($stmt); return;

        $res = array();
        foreach ($stmt as $predefinedlinks_line) {
            $item = array(); // length == 5

            array_push($item , '0000');
            array_push($item , $predefinedlinks_line['alias']);
            array_push($item , $predefinedlinks_line['title']);
            array_push($item , '');
            array_push($item , $predefinedlinks_line['meta_keywords']);

            array_push($res , $item);
        }
        return $res;
    }

    protected function getEstateAdvs() {
        $grid_constructor = new Grid_Constructor();
        $params = array();
        // $params['topic_id'] = 6121;
        // $params['topic_id'] = $topic_id;
        $params['order'] = 'date_added';
        $params['asc'] = 'desc';
        $params['page_limit'] = $this->getConfigValue('apps.smartsearch.ajax_limit');
        // $params['page_limit'] = 5;
        // $params['page']=1;
        $all_advs = $grid_constructor->get_sitebill_adv_ext( $params , false , false );

        $res = array();
        foreach ($all_advs as $key => $value) {
            $item = array(); // length == 5

            $addr_district = $value['district'];
            $addr_street = $value['street'];
            $addr_number = $value['number'];
            $addr_metro = $value['metro'];
            if (!$addr_district)
                $addr_district = '';
            if ($addr_street) $addr_street = ', ' . $addr_street;
            else $addr_street = '';
            if ($addr_number) $addr_number = ', ' . $addr_number;
            $address = $addr_district . $addr_street . $addr_number;

            array_push($item , $value['topic_id']);
            array_push($item , $value['href']);
            array_push($item , $value['name_object']);
            array_push($item , $address);
            array_push($item , $value['metro']);

            array_push($res , $item);
        }
        return $res;
    }

    function getSmartSearchTemplate(){
        global $smarty;
        require_once(SITEBILL_DOCUMENT_ROOT . '/apps/smartsearch/lib/pill.php');
        $smarty->assign('smart_search_title', $this->getConfigValue('apps.smartsearch.title'));

        $pills_odb = $this->getPills();
        $realestate_pills = array();
        if ($pills_odb) {
            foreach ($pills_odb as $pill) {
                $pill_ref = $pill['structure_ref'];
                $pill_obj = new SmartSearchPill($pill_ref, $pill['label'], $pill['abrev'], $pill['singular'], $pill['plural']);
                array_push($realestate_pills, $pill_obj);
            }
        }
        $smarty->assign('smart_search_pills', $realestate_pills);

        $templatePathTheme = SITEBILL_DOCUMENT_ROOT.'/template/frontend/'.$this->getConfigValue('theme').'/smartsearch.tpl';
        $templatePathDefault = SITEBILL_DOCUMENT_ROOT.'/apps/smartsearch/site/template/smartsearch.tpl';

        if( file_exists($templatePathTheme) ){
            return $smarty->fetch($templatePathTheme);
        } else {
            return $smarty->fetch($templatePathDefault);
        }
    }

}
