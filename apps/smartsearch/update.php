<?php
defined('SITEBILL_DOCUMENT_ROOT') or die('Restricted access');
echo('<h1>update.php</h1>');
echo('<h2>smartsearch_update</h2>');

class smartsearch_update extends SiteBill {
    /**
     * Construct
     */
    function __construct() {
        $this->sitebill();
    }
    
    function main() {
        return 'updated';
    }
}
?>
