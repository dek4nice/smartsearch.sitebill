
<main class="smartsearch" id="smartsearch">
  <div class="smartsearch-opener">
    <h2 class="smartsearch-title">{$smart_search_title}</h2>
    <div class="smartsearch-container smartsearch-container__unfocused">
      <div class="smartsearch-inputcnt">
        <input class="smartsearch-input" type="text" autocomplete="off"/>
        <button class="smartsearch-button" type="submit" name="list" onclick="changeViewList(this)"></button>
      </div>
      <ul class="smartsearch-tabs">{foreach from=$smart_search_pills item=pill}
        <li class="smartsearch-tab" role="presentation"><span class="smartsearch-pill" data-type="{$pill->id}">{$pill->plural}</span></li>{/foreach}
      </ul>
      <div class="smartsearch-result js-search-result smartsearch-result__empty">
        <ul class="smartsearch-list"></ul>
      </div>
    </div>
  </div>
</main><link rel="stylesheet" type="text/css" href="{$estate_folder}/apps/smartsearch/front/css/smartsearch.css">
<script type="text/javascript" defer="defer" src="{$estate_folder}/apps/smartsearch/front/js/dexie.min.js"></script>
<script type="text/javascript" defer="defer" src="{$estate_folder}/apps/smartsearch/front/js/SmartSearchMain.js"></script>
<script type="text/javascript" defer="defer" src="{$estate_folder}/apps/smartsearch/front/js/SmartSearchInit.js"></script>