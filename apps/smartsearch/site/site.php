<?php
defined('SITEBILL_DOCUMENT_ROOT') or die('Restricted access');
/**
 * SmartSearch v1.0 frontend (https://bitbucket.org/dek4nice/smartsearch.sitebill)
 * @author Ilyenkov Dmitriy <d3k.web@gmail.com> http://www.ihos.ru
 */
class smartsearch_site extends smartsearch_admin {

    public $smartsearch_url;

    function frontend () {

        $smartsearch_url = '^ssquery';
        if ( !$this->getConfigValue('apps.smartsearch.enable') ) {
            // exit();
            return false;
        }
        
        $REQUESTURIPATH=$this->getClearRequestURI();
        if(!preg_match('/'.$smartsearch_url.'(\/(.*)?)?$/', $REQUESTURIPATH)){
            return false;
        }

        if ( preg_match('/'.$smartsearch_url.'[\/]?$/', $REQUESTURIPATH) && 1==$this->getConfigValue('apps.smartsearch.enable') ) {
            $this->Query();
            exit();
            return true;
        }
        return false;
    }

    function Query() {
        $this->initQueryData();
        $this->exportJsonData();
    }

    function exportJsonData(){
        require_once(SITEBILL_DOCUMENT_ROOT . '/apps/smartsearch/lib/rpc.php');
        $RpcState = new SmartSearchRpc();
        $RpcState->header();

        if (array_key_exists('all', $_GET)) {
            $RpcState->set_array();
            foreach ($this->realestate_pills as $pill_obj) {
                $RpcState->add_pill($pill_obj);
            }
            foreach ($this->realestate_advs as $data_obj) {
                $RpcState->add_data($data_obj);
            }
        } else {
            $RpcState->set_error('Empty query');
        }

        $RpcState->render();
        return;

        // Old GETs
        // =================================================
        if (array_key_exists('id', $_GET)) {
            $RpcState->set_one();
            $id = intval($_GET['id']);
            if (array_key_exists($id , $this->realestate_base)) {
                // server side search disabled
                if (array_key_exists('search', $_GET)) {
                    // search = $_GET['search'];
                    $RpcState->add_data($this->realestate_base[$id]);
                } else {
                    $RpcState->add_data($this->realestate_base[$id]);
                }
            } else {
                $RpcState->set_error('ID not found');
            }
        } elseif (array_key_exists('all', $_GET)) {
            $RpcState->set_array();
            foreach ($this->realestate_base as $pill_obj) {
                $RpcState->add_data($pill_obj);
            }
        } else {
            $RpcState->set_error('Empty query');
        }
        $RpcState->render();
    }

    function initQueryData() {
        require_once(SITEBILL_DOCUMENT_ROOT . '/apps/smartsearch/lib/pill.php');
        $this->realestate_advs = array();
        $this->realestate_pills = array();

        $estate_links = $this->getEstateLinks();
        foreach ($estate_links as $key => $value) {
            array_push($this->realestate_advs, $value);
        }

        $estate_data = $this->getEstateAdvs();
        foreach ($estate_data as $key => $value) {
            array_push($this->realestate_advs, $value);
        }

        $pill = null;
        $pills_odb = $this->getPills();
        foreach ($pills_odb as $pill) {
            $pill_ref = $pill['structure_ref'];
            $pill = new SmartSearchPill($pill_ref, $pill['label'], $pill['abrev'], $pill['singular'], $pill['plural']);
            // $pill->db = $this->getEstateData($pill_ref);
            $this->realestate_pills[$pill_ref] = $pill;
        }

    }

}
