<?php

class SmartSearchRpc {
    // 0 - error
    // 1 - one pill
    // 2 - array of pills
    public $state = null;
    public $message = '';
    public $data = null;
    public $version = null;

    function __construct() {
        $this->state = 0;
        $this->version = '1709';
        $this->set_array();
    }

    function header() {
        header('Content-Type: application/json');
    }

    function set_error($message) {
        $this->state = 0;
        $this->message = $message;
    }

    function set_one() {
        $this->state = 1;
        $this->data = null;
    }

    function set_array() {
        $this->state = 2;
        $this->data = array();
        $this->data['pills'] = array();
        $this->data['advs'] = array();
    }

    function add_pill($data) {
        array_push($this->data['pills'], $data);
    }

    function add_data($data) {
        $this->message = 'Ok';
        if ($this->state == 2) {
            array_push($this->data['advs'], $data);
        } elseif ($this->state == 1) {
            $this->data = $data;
        } else {
            // Null, no error
        }
    }

    function render() {
        echo json_encode($this);
        // echo JSON.stringify($realestate_requested);
    }

}

?>
