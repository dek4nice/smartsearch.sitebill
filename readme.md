
# SmartSearch SiteBill CMS Plugin

## Install

### build

```
git clone https://bitbucket.org/dek4nice/smartsearch.sitebill.git
cd smartsearch.sitebill
npm install
gulp build
```

### move to cms

via copy
``` cp -r ./apps/smartsearch/ /var/www/portal/apps/```

or as linked
``` ln -s ./apps/smartsearch /var/www/portal/apps/smartsearch```

### open /admin
![Open app](https://pp.userapi.com/c837739/v837739840/30f2a/nSey_lVWafs.jpg)

and click **Install**

## Template

add **$smart_search_form** variable to your, for example, *layout_basic.tpl*
```
...
{$smart_search_form}
...
```

## Demo

[https://sitebill--smartsearch.aerobatic.io](https://sitebill--smartsearch.aerobatic.io)

---
> Written with [StackEdit](https://stackedit.io/).
