'use strict';
var l = console.log;
var fs = require('fs');
var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var pug = require('gulp-pug');
var rename = require('gulp-rename');

var path = {
    src: {
        jade: 'src/pug/*.pug',
        php: 'src/php/**/*.php',
        sass: 'src/sass/*.sass',
        js: 'src/js/*.js',
        img: [
            'src/img/**/*.gif',
            'src/img/**/*.jpg',
            'src/img/**/*.png',
            'src/img/**/*.svg',
        ],
    },
    build: {
        php: 'apps/smartsearch/site/template/',
        inc: 'apps/smartsearch/site/inc/',
        css: 'apps/smartsearch/front/css/',
        js: 'apps/smartsearch/front/js/',
        img: 'apps/smartsearch/front/img/',
    },
    watch: {
        jade: 'src/pug/**/*.pug',
        php: 'src/php/**/*.php',
        sass: 'src/sass/**/*.sass',
        js: 'src/js/*.js',
    },
};

function rename_2html(path) {path.extname='.html';}
function rename_2php(path) {path.extname='.php';}
function rename_2tpl(path) {path.extname='.tpl';}
function rename_2null(path) {path.extname='';}

gulp.task('jade:build', function () {
    gulp.src(path.src.jade)
        .pipe(pug({pretty:true}))
        .pipe(rename(rename_2null))
        .pipe(gulp.dest(path.build.php))
});
gulp.task('php:build', function () {
    // duplicate
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.inc))
});
gulp.task('style:build', function () {
    gulp.src(path.src.sass)
        .pipe(sass())
        .pipe(cssmin({keepBreaks: true,}))
        .pipe(gulp.dest(path.build.css))
});
gulp.task('image:build', function () {
    // duplicate
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
});
gulp.task('js:build', function () {
    // duplicate
    gulp.src(path.src.js)
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
});

gulp.task('build', [
    'jade:build',
    'php:build',
    'style:build',
    'image:build',
    'js:build',
]);

gulp.task('jade:demo', function () {
    gulp.src('src/pug/demo.html.pug')
        .pipe(pug())
        .pipe(rename(function (p) {p.basename='index';p.extname='.html';}))
        .pipe(gulp.dest('demo'))
    ;
});
gulp.task('style:demo', function () {
    gulp.src('src/sass/smartsearch.sass')
        .pipe(sass())
        .pipe(cssmin({keepBreaks: true,}))
        .pipe(gulp.dest('demo/css/'))
    ;
});
gulp.task('image:demo', function () {
    gulp.src(path.src.img).pipe(gulp.dest('demo/img/'));
});
gulp.task('js:demo', function () {
    gulp.src('src/js/SmartSearchMain.js').pipe(gulp.dest('demo/js/'));
    gulp.src('src/js/dexie.min.js').pipe(gulp.dest('demo/js/'));
});

gulp.task('demo', [
    'jade:demo',
    'style:demo',
    'image:demo',
    'js:demo'
]);

gulp.task('jade:watch', function () {
    watch(path.watch.jade, function(event, cb) {
        gulp.start('jade:build');
        gulp.start('jade:demo');
    });
});
gulp.task('php:watch', function () {
    watch(path.watch.php, function(event, cb) {
        gulp.start('php:build');
    });
});
gulp.task('js:watch', function () {
    watch(path.watch.js, function(event, cb) {
        gulp.start('js:build');
        gulp.start('js:demo');
    });
});
gulp.task('style:watch', function () {
    watch([path.watch.sass], function(event, cb) {
        gulp.start('style:build');
        gulp.start('style:demo');
    });
});

gulp.task('watch', [
    'jade:watch',
    'php:watch',
    'style:watch',
    'js:watch',
]);

gulp.task('default', [
    'build',
    'demo',
    'watch',
]);

