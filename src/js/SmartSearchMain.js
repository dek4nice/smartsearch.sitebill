// SmartSearch SiteBill CMS Plugin
// version: 1.2.0
// author: Dmitriy Ilyenkov
// home: https://bitbucket.org/dek4nice/smartsearch.sitebill

var SmartSearchIndexedDB = function() {
    this.schema = {
        objects: "++id,type",
        types: "id",
    }
    this.innerLog = function(name , value) {
        // console.log('::>' , name , value);
    }
    this.init = function() {
        this.innerLog('init');
        this.name = "realestate";
        this.DB = new Dexie(this.name);
        this.name_local_types = this.name + '_types_cnt';
        this.name_local_objects = this.name + '_objects_cnt';
        this.DB.version(1).stores(this.schema);
        this.DB.open();
        // this.checkout();
    }
    this.init_content_objects = function(data) {
        var idb_this = this;
        idb_this.innerLog('init_content_objects');
        for (var i = 0; i < data.length; i++) {
            line = data[i];
            idb_object = {
                type: Number(line[0]),
                url: line[1].startsWith('/') ? line[1] : '/'+line[1],
                title: line[2],
                address: line[3],
                metro: line[4],
            };
            idb_this.DB.objects.add(idb_object);
        };
        idb_this.DB.objects.count(function (cnt) {
            idb_this.innerLog('objects count' , cnt);
            window.localStorage.setItem(idb_this.name_local_objects , cnt);
        });
    }
    this.init_content_types = function(data) {
        var idb_this = this;
        idb_this.innerLog('init_content_types');
        for (var i = 0; i < data.length; i++) {
            idb_this.DB.types.add(data[i]);
        };
        idb_this.DB.types.count(function (cnt) {
            idb_this.innerLog('types count' , cnt);
            window.localStorage.setItem(idb_this.name_local_types , cnt);
        });
    }
    this.load_data = function () {
        this.innerLog('load_data');
        this.clear_data_types();
        this.clear_data_objects();
        this.init_content_objects();
        this.init_content_types();
    }
    this.clear_data_objects = function() {
        this.innerLog('clear_data_objects');
        this.DB.objects.clear();
        window.localStorage.removeItem(this.name_local_objects);
    }
    this.clear_data_types = function() {
        this.innerLog('clear_data_types');
        this.DB.types.clear();
        window.localStorage.removeItem(this.name_local_types);
    }
    this.gets_all_objects = function() {
        this.innerLog('gets_all_objects');
        this.DB.objects.each(function(line) {
            console.log("Found: " + line.url);
        });
    }
    this.gets = function() {
        this.innerLog('gets');
        this.DB.objects.get(2 , function(line) {
            console.log("Found: " + line.url);
        });
        this.DB.objects.where("type").equals(6122).each(function(line) {
            console.log("Found: " + line.url);
        });
        this.DB.types.where("id").equalsIgnoreCase("6122").first(function(line) {
            console.log("Found: " + line.singular);
        }).catch(function (error) {
            console.error(error);
        });
    }
    this.count_objects = function () {
        this.innerLog('count_objects');
        return this.DB.objects.count(function (idbCnt) {
            var localCnt = window.localStorage.getItem(this.name_local_objects);
            counter_check_status = !(!localCnt || localCnt != idbCnt);
            console.log('counter_check_status: ' , localCnt , idbCnt);
            return counter_check_status;
        });
    }
    this.count_types = function (resolve , reject) {
        var idb_this = this;
        idb_this.innerLog('count_types');
        return idb_this.DB.types.count(function (idbCnt) {
            var localCnt = window.localStorage.getItem(idb_this.name_local_types);
            counter_check_status = !(!localCnt || localCnt != idbCnt);
            if (resolve && !counter_check_status) {
                resolve();
            }
            return counter_check_status;
        });
    }
    this.count_objects = function (resolve , reject) {
        var idb_this = this;
        idb_this.innerLog('count_objects');
        return idb_this.DB.objects.count(function (idbCnt) {
            var localCnt = window.localStorage.getItem(idb_this.name_local_objects);
            counter_check_status = !(!localCnt || localCnt != idbCnt);
            if (resolve && !counter_check_status) {
                resolve();
            }
            return counter_check_status;
        });
    }
    this.checkout_objects = function (data) {
        var idb_this = this;
        idb_this.innerLog('checkout_objects');
        idb_this.count_objects(function(status) {
            idb_this.clear_data_objects();
            idb_this.init_content_objects(data);
        });
    }
    this.checkout_types = function (data) {
        var idb_this = this;
        idb_this.innerLog('checkout_types');
        idb_this.count_types(function(status) {
            idb_this.clear_data_types();
            idb_this.init_content_types(data);
        });
    }
    this.checkout = function () {
        this.innerLog('checkout');
        var checked = true;
        Promise.all([this.count_objects(),this.count_types()]).then(function(rez) {
            // console.log('::> rez: ' , rez);
            if (!rez[0]||!rez[1]) {
                this.load_data();
            }
        });
    }
    this.query_all_each = function (callback , last) {
        var idb_this = this;
        // window.smartsearch.realdb.DB.objects.orderBy('type').each(function(fn){console.log('---',fn);});
        return idb_this.DB.objects.orderBy('type').each(callback).then(last);
        // return idb_this.DB.objects.each(callback).then(last);
    }
    this.filter = function (query) {
        this.innerLog('filter');
    }
}

// var SmartEstateContainer = function() {}
var SmartSearchResultList = function() {

    var ul_this = this;

    var SmartSearchLiElement = function(item) {
        var li_this = this;
        li_this.init = function(item) {
            ul_this.log('li.init');
            li = document.createElement('li');
            li.classList.add('smartsearch-item');
            li.setAttribute('data-type' , 211);
            a = document.createElement('a');
            a.classList.add('smartsearch-link');
            a.classList.add('smartsearch-link__' + item['type']);
            span = document.createElement('span');
            a.innerText = item['title'];
            span.classList.add("small");
            span.innerText = item['address'];
            a.href = item['url'];
            a.appendChild(span);
            a.setAttribute('target' , "_blank");
            li.appendChild(a);
            return li;
        }
        li_this.hide = function() {
            ul_this.log('li.hide');
            li_this.node.classList.add(ul_this.itemhidden);
            ul_this.hidden_counter++;
        }
        li_this.show = function() {
            ul_this.log('li.show');
            li_this.node.classList.remove(ul_this.itemhidden);
            ul_this.hidden_counter--;
        }
        li_this.node = li_this.init(item);
    }

    ul_this.init = function(node) {
        ul_this.node = node;
        ul_this.itemhidden = 'smartsearch-item__hidden'
        ul_this.hidden_counter = 0;
        ul_this.more = null;
        ul_this.more_limit = 20;
        // ul_this.init();
        // ul_this.node.addEventListener('scroll', ul_this.catch_scroll);
        ul_this.node.addEventListener('wheel', ul_this.catch_scroll);
    }
    ul_this.log = function(value) {
        // console.log('SmartSearchResultList.' + value);
    }
    ul_this.catch_scroll = function(e) {
      // if (e.target.tagName != 'smartsearch-result') return;
      var area = e.target.parentElement.parentElement;
      if (!area.classList.contains('smartsearch-result')) return;
      // if (!area.classList.contains('smartsearch-list')) return;

      var delta = e.deltaY || e.detail || e.wheelDelta;

      // console.log(delta, area.scrollHeight, area.clientHeight, area.scrollTop);

      if (delta < 0 && area.scrollTop == 0) {
        e.preventDefault();
      }

      if (delta > 0 && area.scrollHeight - area.clientHeight - area.scrollTop <= 1) {
        e.preventDefault();
      }
    };
    ul_this.create_li = function(item) {
        ul_this.node.parentElement.classList.remove('smartsearch-result__empty');
        li = new SmartSearchLiElement(item);
        ul_this.node.appendChild(li.node);
        if (ul_this.node.children.length > ul_this.more_limit) {
            li.hide();
        }
        return li;
    }
    ul_this.create_more = function() {
        ul_this.log('create_more');
        li = document.createElement('li');
        li.classList.add('smartsearch-item');
        li.classList.add('smartsearch-item__more');
        a = document.createElement('a');
        a.classList.add('smartsearch-link');
        span = document.createElement('span');
        a.innerText = 'show more';
        a.innerText = 'показать ещё';
        a.href = '#more';
        a.onclick = function() {
            ul_this.show_next(false);
        }
        li.appendChild(a);
        ul_this.more = li;
        ul_this.node.appendChild(li);
        ul_this.more_update();
    }
    ul_this.kill_more = function() {
        ul_this.node.removeChild(ul_this.more);
        ul_this.more = null;
    }
    ul_this.show_next = function(all) {
        ul_this.log('show_next');
        // var items = ul_this.node.getElementsByClassName(ul_this.itemhidden);
        // for (var i = 0; i < (all ? items.length : Math.min(items.length , ul_this.more_limit)); i++) {
        for (var i = 0 , h = 0; i < ul_this.node.children.length; i++) {
            // console.log('a' , items[i]);
            if (ul_this.node.children[i].classList.contains(ul_this.itemhidden)) {
                if (h < ul_this.more_limit) {
                    h++;
                    ul_this.node.children[i].classList.remove(ul_this.itemhidden);
                    ul_this.hidden_counter--;
                }
            }
            // console.log('b' , items[i]);
        };
        ul_this.more_update();
    }
    ul_this.clear = function() {
        ul_this.node.parentElement.classList.add('smartsearch-result__empty');
        ul_this.log('clear');
        ul_this.node.innerHTML = '';
        ul_this.hidden_counter = 0;
    }
    ul_this.more_update = function() {
        if (ul_this.hidden_counter == 0) {
            ul_this.kill_more();
        }
    }
}

var SmartSearchApp = function(root) {
    // Ajax or IndexDB ??
    root = document.getElementById(root);
    this.ajax = false;
    this.idb_dbname = 'realestate';
    this.idb_tbname = '';
    this.realdb = new SmartSearchIndexedDB();
    this.root = root;
    var gbc = function(classname) {
        el = root.getElementsByClassName(classname);
        if (el.length) return el[0];
        return null;
    };
    this.list = new SmartSearchResultList();
    this.list.init(gbc('smartsearch-list'));
    this.cnt = gbc('smartsearch-container');
    this.input = gbc('smartsearch-input');
    this.tabs = gbc('smartsearch-tabs');
    this.pills = this.tabs.getElementsByClassName('smartsearch-pill');
    // this.url = '/form/query.php';
    // this.url = '/query.php';
    this.url = '/ssquery';
    // this.url = '/ssquery.json';
    this.type = '';
    this.caption_default = 'Название объекта, улицы или района';
    this.caption_notfound = 'Под данному запросу ничего не найдено, задайте новый';
    this.initBindings();
    this.search_time_marker = null;
    this.ajax_post = null;
    this.freeze = false;
    this.answer = '';
}
SmartSearchApp.prototype.value = function() {
    var val = this.input.value.trim();
    if (val == this.caption_default)
        return '';
    else
        return val;
};
SmartSearchApp.prototype.isempty = function(first_argument) {
    return this.input.value.trim() == '';
};
SmartSearchApp.prototype.innerLog = function(message) {
    // Error('>>' , message);
    // console.log('#>' , message);
};
SmartSearchApp.prototype.initBindings = function() {
    var self = this;
    self.innerLog('initBindings');
    self.input.onfocus = function() {self.onInputFocus()};
    self.input.onblur = function() {self.onInputBlur()};
    document.body.onclick = function() {self.onBodyClick()};
    self.cnt.onclick = function(e) {self.onContainerClick(e)};
    self.input.value = self.caption_default;
    self.input.oninput = function() {self.onInputTyping()};
    Array.prototype.forEach.call(self.pills , function(e) {
        e.onclick = function() {self.onTabClick(e)};
    });
    // this.tabDefault();
};
SmartSearchApp.prototype.onContainerClick = function(e) {
    this.innerLog('onContainerClick');
    e.stopPropagation();
};
SmartSearchApp.prototype.onBodyClick = function(e) {
    this.innerLog('onBodyClick');
    // this.tabs
    this.searchResultClose();
};
SmartSearchApp.prototype.onTabClick = function(target , init) {
    this.innerLog('onTabClick');
    this.tabDeactivate();
    this.type = target.attributes['data-type'].value;
    target.parentElement.classList.add('active');
    if (!init)
        this.loadSearchResultSelected();
    // .smartsearch-pill
};
SmartSearchApp.prototype.tabDefault = function() {
    this.innerLog('tabDefault');
    // console.log(this.pills[0]);
    // this.tabDeactivate();
    this.onTabClick(this.pills[0] , true);
};
SmartSearchApp.prototype.tabDeactivate = function() {
    this.innerLog('tabDeactivate');
    this.type = null;
    Array.prototype.forEach.call(this.pills , function(e) {
        e.parentElement.classList.remove('active');
        // console.log('pill:' , e);
        // e.onclick = () => {this.onTabClick(e)};
    });
};
SmartSearchApp.prototype.onInputFocus = function() {
    this.innerLog('onInputFocus');
    this.inputClear();
    this.loadSearchResultSelected();
    this.containerUncollapse();
    // el.addClass('focus');
};
SmartSearchApp.prototype.inputClear = function() {
    this.innerLog('inputClear');
    // var caption_default = this.input.attributes.getNamedItem('def');
    this.input.value = this.value();
};
SmartSearchApp.prototype.onInputBlur = function(el) {
    this.innerLog('onInputBlur');
    if (this.input.value.trim() == '') {
        this.input.value = this.caption_default;
    }
};
SmartSearchApp.prototype.onInputTyping = function(e) {
    var self = this;
    self.innerLog('onInputTyping');
    clearTimeout(self.search_time_marker);
    self.search_time_marker = setTimeout(function() {self.loadSearchResultSelected()}, 100);
};
SmartSearchApp.prototype.containerCollapse = function() {
    this.innerLog('containerCollapse');
    if (!this.freeze)
        this.cnt.classList.add('smartsearch-container__unfocused');
};
SmartSearchApp.prototype.containerUncollapse = function() {
    this.innerLog('containerUncollapse');
    this.cnt.classList.remove('smartsearch-container__unfocused');
};
SmartSearchApp.prototype.containerFreeze = function() {
    this.innerLog('containerFreeze');
    this.freeze = true;
    this.cnt.classList.add('smartsearch-container__loading');
    // setTimeout( () => {this.containerUnfreeze();} , 2000 );
};
SmartSearchApp.prototype.containerUnfreeze = function() {
    this.innerLog('containerUnfreeze');
    this.freeze = false;
    this.cnt.classList.remove('smartsearch-container__loading');
};
SmartSearchApp.prototype.searchResultClose = function() {
    this.innerLog('searchResultClose');
    this.searchResultClear();
    this.containerCollapse();
};
SmartSearchApp.prototype.searchResultClear = function() {
    this.innerLog('searchResultClear');
    if (!this.freeze)
        this.list.clear();
};
SmartSearchApp.prototype.searchResultUpdateSingle = function(more) {
    this.innerLog('searchResultUpdateSingle');
    var elem = null;
    if (!this.answer) {
        console.error(this.answer);
        return;
    }
    var c = 0;
    value = this.value().toLowerCase();

    for (var i = 0; i < this.answer.db.length; i++) {
        if (value) {
            text = '';
            text += this.answer.db[i][1];
            text += this.answer.db[i][2];
            if (text.toLowerCase().search(value) == -1)
                continue;
            else
                c++;
        } else {
            c++;
        }
        elem = this.list.create_li(this.answer.db[i]);
        if (!more && c > 5)
            elem.classList.add(this.itemhidden);
        // console.log('e' , elem);
    };
    this.list.create_more();
    // console.log(this.answer);
    // if (this.value())
        // this.filterResults();
};
SmartSearchApp.prototype.searchResultUpdateAll = function(more) {
    var self = this;
    self.innerLog('searchResultUpdateAll');
    // if (!self.answer) {
        // console.error(self.answer);
        // return;
    // }
    var elem = null;
    var fulltext = '';
    var c = 0;
    value = self.value().toLowerCase();

    self.realdb.query_all_each(function(line) {
        // console.log('query outer' , line);
        if (value) {
            fulltext = '';
            fulltext += ' ' + line['title'];
            fulltext += ' ' + line['address'];
            fulltext += ' ' + line['metro'];
            if (fulltext.toLowerCase().search(value) == -1)
                // continue;
                return;
            else
                c++;
        } else {
            c++;
        }
        elem = self.list.create_li(line);
        if (!more && c > 5)
            elem.hide();
            // this.list.hide(self, elem);
    } , function() {
        self.list.create_more();
    });
    return;
    for (var i = 0; i < Things.length; i++) {
        Things[i]
    };
    for (var i = 0; i < self.answer.length; i++) {
        for (var y = 0; y < self.answer[i].db.length; y++) {
        };
    };
};
SmartSearchApp.prototype.PythonMarker = function() {
    var self = this;
    self.innerLog('PythonMarker');
    self.inputClear();
    self.searchResultClear();
    self.containerUncollapse();
    self.searchResultUpdateAll(false);
};
SmartSearchApp.prototype.answerUpdate = function() {
    var self = this;
    self.innerLog('answerUpdate');
    if (!self.answer) {
        console.error(Error(self.answer));
        return;
    }
    // console.log('here:' , this.answer.advs[100]);
    self.realdb.checkout_types(this.answer.pills);
    self.realdb.checkout_objects(this.answer.advs);

    // setTimeout(() => {self.PythonMarker()}, 500);

    return;
    for (var i = 0; i < self.answer.pills; i++) {
    };
    var c = 0;
    value = self.value().toLowerCase();

    for (var i = 0; i < self.answer.length; i++) {
        for (var y = 0; y < self.answer[i].db.length; y++) {
            console.log(self.answer[i].db[y]);
            // elem = self.list.create_li(line);
            // self.list.appendChild(elem);
        };
    };
    // this.list.create_more();
    // console.log(self.answer);
    // if (self.value())
        // self.filterResults();
};
SmartSearchApp.prototype.loadSearchResultSelected = function() {
    this.innerLog('loadSearchResultSelected');
    var user_inputed = this.value();
    this.searchResultClear();
    // this.containerFreeze();

    if ( user_inputed.length >= 1 || this.type ) {
        // this.containerFreeze();
        var param = {
            id: this.type,
            query: user_inputed,
        };
        // this.loadSyncAjax(param);
        // this.searchResultUpdateFiltered(param);
        this.searchResultUpdateAll(true);
    }
    else {
        this.inputClear();
        this.searchResultClear();
    }
};
SmartSearchApp.prototype.getAsyncAjax = function(url) {
    var self = this;
    self.innerLog('getAsyncAjax');
    sub_ajax = function(resolve, reject) {
        var xhr_req = new XMLHttpRequest();
        xhr_req.open('GET', url);

        xhr_req.onload = function() {
            if (xhr_req.status == 200) {
                resolve(xhr_req.response);
            } else {
                reject(Error(xhr_req.statusText));
            }
        };

        // отлавливаем ошибки сети
        xhr_req.onerror = function() {
            reject(Error("Network Error"));
        };

        // Делаем запрос
        xhr_req.send();
    }
    return new Promise(sub_ajax);
};
SmartSearchApp.prototype.loadAsyncAjax = function(param) {
    var self = this;
    self.innerLog('loadAsyncAjax');
    if (!self.ajax_post) {
        self.ajax_post = true;
        self.getAsyncAjax(self.url).then(JSON.parse).then(function(response) {
          console.log("JSON: ", response);
        });
    }
};
SmartSearchApp.prototype.loadSyncAjax = function() {
    var self = this;
    self.innerLog('loadSyncAjax');
    if (!self.ajax_post) {
        self.ajax_post = true;
        var httpRequest = new XMLHttpRequest();
        // httpRequest.open("GET", this.url+"?id="+param.id, true);
        // httpRequest.open("GET", this.url, true);
        httpRequest.open("GET", this.url+"?all", true);
        httpRequest.responseType = 'json';
        httpRequest.onreadystatechange = function() {
            var DONE = 4;
            var OK = 200;
            if (httpRequest.readyState === DONE) {
                if (httpRequest.status === OK) {
                    self.answer = httpRequest.response.data;

                    // Ajax search result
                    if (httpRequest.response.state == 0) {
                        // console.error(httpRequest.responseText.message);
                    }
                    if (httpRequest.response.state == 1) {
                        self.searchResultUpdateSingle(false);
                        // self.searchResultUpdateSingle(httpRequest.responseText.data);
                    }
                    if (httpRequest.response.state == 2) {
                        // self.searchResultUpdateAll(false);
                        self.answerUpdate();
                        // console.log(httpRequest.response.state);
                    }
                    // return response;
                } else {
                    console.error('Error: ' + httpRequest.status);
                }
                self.ajax_post = false;
            }
            self.containerUnfreeze();
            // self.ajax_post = false;
        };
        httpRequest.send();
    }
};
SmartSearchApp.prototype.filterResults = function() {
    this.innerLog('filterResults');
    var items = this.list.getElementsByClassName('smartsearch-item');
    for (var i = 0; i < items.length; i++) {
        this.innerLog('filterResults: ' + items[i]);
    };
};
SmartSearchApp.prototype.startLazyLoading = function() {
    var self = this;
    self.innerLog('startLazyLoading');
    if (self.ajax) {
        return false;
    } else {
        self.realdb.init();
    }

    now = function() {return Math.floor(Date.now() / 1000)};
    var timestamp = localStorage.getItem('realestate_ts');
    if (!timestamp) {
        localStorage.setItem('realestate_ts' , now());
        this.containerFreeze();
        this.loadSyncAjax();
    } else {
        var timestamp = localStorage.getItem('realestate_ts');
        var ts_diff = now() - timestamp;
        self.innerLog('timestamp diff: ' + ts_diff);
        if (ts_diff > 86400) {
            // here
            localStorage.setItem('realestate_ts' , now());
            this.containerFreeze();
            this.loadSyncAjax();
        } else {
            // dummy
        }
    }
};

window.addEventListener("load", function() {
    // console.log('document loaded');
});
document.addEventListener("DOMContentLoaded", function() {
    // console.log('DOM Ready');
});
