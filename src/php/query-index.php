<?php
/**
 * QUERY API
 * @author Ilyenkov Dmitriy <d3k.web@gmail.com> http://www.ihos.ru
 */

ini_set('display_errors','On');

session_start();

$sitebill_document_root = $_SERVER['DOCUMENT_ROOT'].$folder;
define('SITEBILL_DOCUMENT_ROOT', $sitebill_document_root);

require_once(SITEBILL_DOCUMENT_ROOT."/inc/db.inc.php");

$settings=parse_ini_file(SITEBILL_DOCUMENT_ROOT.'/settings.ini.php',true);
if (isset($settings['Settings']['estate_folder'])AND($settings['Settings']['estate_folder']!='')) {
    $folder='/'.$settings['Settings']['estate_folder'];
} else {
    $folder='';
}

define('SITEBILL_MAIN_URL', $folder);
define('DB_PREFIX', $__db_prefix);

ini_set("include_path", $include_path );
require_once(SITEBILL_DOCUMENT_ROOT.'/third/smarty/Smarty.class.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/system/init.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/db/MySQL.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/sitebill.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/sitebill_krascap.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/admin/object_manager.php');
// require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/language/russian.php');
// require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/system/install/install.php');

$smarty = new Smarty;

// $init = new Init();
// $init->initGlobals();

// require_once SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/system/multilanguage/multilanguage.class.php';
// Multilanguage::start('frontend',$_SESSION['_lang']);

require_once(SITEBILL_DOCUMENT_ROOT.'/apps/api/classes/class.controller.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/api/classes/class.common.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/api/classes/class.static_data.php');
require_once(SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/admin/structure/structure_manager.php');
require_once SITEBILL_DOCUMENT_ROOT.'/apps/system/lib/frontend/grid/grid_constructor.php';
require_once('inc/query-app.php');
// $sitebill = new SiteBill();

$smartsearchquery = new SmartSearchQuery();
// $smartsearchquery->get_sales_grid();

// $sitebill_krascap = new SiteBill_Krascap();
// $sitebill_krascap->main();
// exit;
?>
